import hashlib
import xlrd

# opening the workbook
workbook = xlrd.open_workbook("hashlib.xls")
worksheet = workbook.sheet_by_name("Sheet1")

# getting number of rows from excel sheet
num_rows = worksheet.nrows  # Number of Rows
num_cols = worksheet.ncols  # Number of Columns

# chain is ok
blockchain = True

row = 1
col = 0
while blockchain and row < num_rows:
    data = worksheet.cell_value(row, col)
    key = worksheet.cell_value(row, col + 1)
    hash1 = worksheet.cell_value(row, col + 2)
    data_full = str(data) + str(key)
    hash_new = hashlib.sha256((str(data) + str(key)).encode('utf-8')).hexdigest()
    if row > num_rows:
        break
    elif hash1 == hash_new:
        row += 1
    else:
        blockchain = False
        print("value error at row ", row)

print("Done with status =", blockchain)
print(hash1)
