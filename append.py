from xlutils.copy import copy
import xlrd
import hashlib
import datetime

# making a workbook
workbook_original = xlrd.open_workbook('hashlib.xls')
worksheet_old = workbook_original.sheet_by_name("Sheet1")
workbook = copy(workbook_original)
worksheet = workbook.get_sheet(0)

# todo: hash generated make it to connect with other files

# find existing row number
row = worksheet_old.nrows  # Number of Rows
col = 0  # initial column
hash1 = worksheet_old.cell_value(row - 1, col + 2)
row -= 1
for x in range(0, 10):
    row = row + 1
    name = "Aditya" + str(x)
    date = datetime.datetime.now()
    data = str(hash1) + str(date) + name
    key = -1
    hash1 = hashlib.sha256(str(data).encode('utf-8')).hexdigest()
    hash2 = hash1
    hash3 = hash1
    while hash3[:4] != "0000":
        key = key + 1
        hash3 = hashlib.sha256((str(hash1) + str(key)).encode('utf-8')).hexdigest()
        if key > 9999999999:
            break
    # writing data sheet
    worksheet.write(row, col, hash2)
    worksheet.write(row, col + 1, str(key))
    worksheet.write(row, col + 2, hash3)
    worksheet.write(row, col + 3, str(date))
    worksheet.write(row, col + 4, name)
    print("the key is %d and the hash is %s" % (key, hash3))
workbook.save('hashlib.xls')
